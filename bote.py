import requests
import time
import json
import datetime


edit_ids = {}
DEBUG = False


def edit_text(page="", text="", print_status=True):
    '''Edit the contents of a page.'''
    r = requests.post("http://htwins.net/edit/" + page, data={'content': text})

    if r.status_code + r.reason != "200 OK":
        print("Unexpected status code. Abort!")
        exit()

    if print_status:
        print(r.status_code, r.reason)


def read_data(page="", dual_data=False, print_output=True):
    '''Get the contents of a page. If dual data is enabled,
       the zeroth element is the full data, and the first is only text.'''

    r = requests.get("http://htwins.net/edit/submit/" + page,
                     timeout=15,
                     headers={'Cache-Control': 'nocache', 'Pragma': 'nocache'})
    data = r.json()

    edit_id = data['editId']
    ip = data['ipAddress']
    last_modified = datetime.datetime.utcfromtimestamp(data['lastModified']).strftime('%Y-%m-%dT%H:%M:%SZ')
    content = data['content']

    output = f"#{edit_id} by {ip} at {last_modified} UTC: {content}"
    if dual_data:
        output = [output, content]

    def output_check(output):
        edit_ids[page] = edit_id
        if dual_data: print(output[0])
        else: print(output)

    if print_output:
        try:
            if edit_id != edit_ids[page]: output_check(output) 
        except KeyError:
            output_check(output)

    return output


while True:
    content = 'two can play at the bot game\n\n'
    content += '<3 from htwins stem\n\n'
    content += 'https://discord.gg/dxCBaYn\n\n'
    content += 'shoutouts to /attractions'

    read = read_data(page="", dual_data=True, print_output=False)

    if DEBUG:
        print(read)
        print(content)

    if read[1] != content:
        print("Overwriting: " + read[1])
        edit_text("", content, False)

    time.sleep(5)
